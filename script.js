const alertTrue = "You are welcome";
const alertFalse = "Потрібно ввести однакові значення";
const inputPassword = document.querySelector('input');
const confirmPassword = document.querySelectorAll('input')[1];
const inputPasswordIcons = [document.querySelector('.fa-eye'), document.querySelector('.fa-eye-slash')];
const confirmPasswordIcons = [document.querySelectorAll('.fa-eye')[1], document.querySelectorAll('.fa-eye-slash')[1]];
const buttonSubmit = document.querySelector('button');

function displayNone(elVisible, elHidden){
    elVisible.classList.toggle('hidden');
    elHidden.classList.toggle('hidden');
}

function type(input){
    if (input.type == "password") input.type = "text";
    else input.type = "password";
}

function inputPasswordEventListener(passwordIcons, password, i){
    if (i == 0){
        passwordIcons[i].addEventListener("click", () => {
            displayNone(passwordIcons[i], passwordIcons[i + 1]);
            type(password);
        });
    }
    else if(i == 1){
        passwordIcons[i].addEventListener("click", () => {
            displayNone(passwordIcons[i], passwordIcons[i - 1]);
            type(password);
        });
    }
}

inputPasswordEventListener(inputPasswordIcons, inputPassword, 0);
inputPasswordEventListener(inputPasswordIcons, inputPassword, 1);
inputPasswordEventListener(confirmPasswordIcons, confirmPassword, 0);
inputPasswordEventListener(confirmPasswordIcons, confirmPassword, 1);

buttonSubmit.onclick = () => {
    if(inputPassword.value === confirmPassword.value) alert(alertTrue);
    else alert(alertFalse);
    return false;
}

// Выбираем кнопку
const btn = document.querySelector(".btn-toggle");
const label1 = document.querySelectorAll("label")[0];
const label2 = document.querySelectorAll("label")[1];
const inputWrapper1 = document.querySelectorAll(".input-wrapper input")[0];
const inputWrapper2 = document.querySelectorAll(".input-wrapper input")[1];
// Выбираем настройки темы из localStorage
const currentTheme = localStorage.getItem("theme");
// Если текущая тема в localStorage равна "dark"…
if (currentTheme == "dark") {
  // …тогда мы используем класс .dark-theme
  document.body.classList.add("dark-theme");
  btn.classList.add("dark-theme");
  inputWrapper1.classList.add("dark-theme");
  inputWrapper2.classList.add("dark-theme");
  label1.classList.add("dark-theme");
  label2.classList.add("dark-theme");
  buttonSubmit.classList.add("dark-theme");
}

// Отслеживаем щелчок по кнопке
btn.addEventListener("click", function() {
  // Переключаем класс .dark-theme при каждом щелчке
  document.body.classList.toggle("dark-theme");
  btn.classList.toggle("dark-theme");
  inputWrapper1.classList.toggle("dark-theme");
  inputWrapper2.classList.toggle("dark-theme");
  label1.classList.toggle("dark-theme");
  label2.classList.toggle("dark-theme");
  buttonSubmit.classList.toggle("dark-theme");
  // Допустим, тема светлая
  let theme = "light";
  // Если <body> содержит класс .dark-theme…
  if (document.body.classList.contains("dark-theme")) {
    // …тогда делаем тему тёмной
    theme = "dark";
  }
  // После чего сохраняем выбор в localStorage
  localStorage.setItem("theme", theme);
});